create table if not exists dictionary (
    id float not null
        constraint dictionary_pkey primary key,
    word varchar(255),
    translation text
);
create sequence if not exists hibernate_sequence;