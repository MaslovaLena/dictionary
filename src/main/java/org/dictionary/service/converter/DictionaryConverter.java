package org.dictionary.service.converter;

import org.dictionary.domain.dto.DictionaryDto;
import org.dictionary.domain.entity.Dictionary;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DictionaryConverter implements Converter<Dictionary, DictionaryDto> {
    private final ConversionService conversionService;

    public DictionaryConverter(@Lazy ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    public DictionaryDto convert(Dictionary source) {
        DictionaryDto target = new DictionaryDto();
        target.setId(source.getId());
        target.setWord(source.getWord());
        target.setTranslation(source.getTranslation());
        return target;
    }
}
