package org.dictionary.service;

import org.dictionary.domain.dto.DictionaryDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DictionaryService {
    Page<DictionaryDto> findAll(Pageable pageable);
    DictionaryDto save(DictionaryDto dictionaryDto);
}
