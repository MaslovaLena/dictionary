package org.dictionary.service.impl;

import lombok.AllArgsConstructor;
import org.dictionary.domain.dto.DictionaryDto;
import org.dictionary.domain.entity.Dictionary;
import org.dictionary.repository.DictionaryRepository;
import org.dictionary.service.DictionaryService;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DictionaryServiceImpl implements DictionaryService {
    private final DictionaryRepository repository;
    private final ConversionService conversionService;

    @Override
    public Page<DictionaryDto> findAll(Pageable pageable) {
        return repository.findAll(pageable)
                .map(dictionary -> conversionService.convert(dictionary, DictionaryDto.class));
    }

    @Override
    public DictionaryDto save(DictionaryDto dictionaryDto) {
        Dictionary dictionary = conversionService.convert(dictionaryDto, Dictionary.class);
        return conversionService.convert(repository.save(dictionary), DictionaryDto.class);
    }
}
