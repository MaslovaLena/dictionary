package org.dictionary.configuration.postgres;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Primary
@Configuration
public class PostgresqlConfig {

    public static final String POSTGRES_DATASOURCE_BEAN_NAME = "postgresql";

    @Primary
    @Bean(name = POSTGRES_DATASOURCE_BEAN_NAME)
    @ConfigurationProperties(prefix = "datasource.postgresql")
    public DataSource postgresDataSource() {
        return new HikariDataSource();
    }
}
