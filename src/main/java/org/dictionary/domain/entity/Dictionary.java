package org.dictionary.domain.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Dictionary {
    @Id
    @GeneratedValue
    private Long id;
    private String word;
    private String translation;
}
