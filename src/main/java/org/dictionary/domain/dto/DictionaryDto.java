package org.dictionary.domain.dto;

import lombok.Data;

@Data
public class DictionaryDto {
    private Long id;
    private String word;
    private String translation;
}
