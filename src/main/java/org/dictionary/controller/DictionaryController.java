package org.dictionary.controller;

import lombok.AllArgsConstructor;
import org.dictionary.domain.dto.DictionaryDto;
import org.dictionary.service.DictionaryService;
import org.dictionary.service.impl.DictionaryServiceImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RestController
@AllArgsConstructor
@RequestMapping(path = "/v1/dictionary", produces = APPLICATION_JSON_VALUE)
public class DictionaryController {

    private final DictionaryService service;

    @GetMapping
    public ResponseEntity<Page<DictionaryDto>> findDictionary(Pageable pageable) {
        return ResponseEntity.ok(service.findAll(pageable));
    }

    @PostMapping
    public ResponseEntity<DictionaryDto> save(@RequestBody DictionaryDto dictionaryDto) {
        DictionaryDto created = service.save(dictionaryDto);
        return ResponseEntity.created(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
        .buildAndExpand(created.getId()).toUri()).body(created);
    }
}
