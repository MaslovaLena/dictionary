package org.dictionary.repository;

import org.dictionary.domain.entity.Dictionary;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DictionaryRepository extends PagingAndSortingRepository<Dictionary, Long> {
}
